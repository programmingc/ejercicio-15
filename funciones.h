#ifndef FUNCIONES_H_INCLUDED
#define FUNCIONES_H_INCLUDED

int crearArchivo(char *);
int leerYMostrarArchivo(char *);
int buscarEnArchivo(char *, char *, int *, int *, int *, int *);
int longitudCadena(char *);


#endif // FUNCIONES_H_INCLUDED
