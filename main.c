#include <stdio.h>
#include <stdlib.h>
#include "funciones.h"


int main()
{
    char data[100], cade[20];
    int res = 0;
    int posicionFila = 0, posicionLinea = 0, posicionAux = 0;

    crearArchivo("ejercicio15.txt");

    puts("Ingrese palabra a ser buscada");
    fgets(cade, 20, stdin);


    FILE *pf = fopen("ejercicio15.txt", "r");

    if(!pf)
    {
        printf("No se pudo abrir el archivo");
        return -1;
    }

    fgets(data, 100, pf);

    while(!feof(pf))
    {
        res = buscarEnArchivo(data, cade, &res, &posicionFila, &posicionLinea, &posicionAux);
        //printf("%s", data );
        fgets(data, 100, pf);
    }

    if (!res)
    {
        printf("\nPalabra no encontrada\n");
        return 0;
    }
    printf("\nPalabra encontrada en la linea %d y en la posición %d\n", posicionFila, posicionLinea);

}
