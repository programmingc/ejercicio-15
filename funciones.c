#include <stdio.h>
#include <stdlib.h>
#include "funciones.h"

int crearArchivo(char * texto)
{
    FILE *fp=fopen(texto,"wt");
    char cad[50];
    if(!fp)
    {
        puts("No se pudo abrir");
        return -1;
    }
    puts("Cargar archivo\n");
    for(int i=0; i<3; i++)
    {
        fgets(cad,50,stdin);
        fputs(cad,fp);
    }
    fclose(fp);

    return 2;
}

int leerYMostrarArchivo(char * texto)
{
    char cad1[50];
    FILE*fp=fopen(texto,"rt");
    if (!fp)
    {
        puts("No se pudo leer el Archivo");
        return -1;
    }
    puts("\nLa cadena tomada del archivo es\n");
    fgets(cad1,50,fp);
    while (!feof(fp))
    {
        printf("%s",cad1);
        fgets(cad1,50,fp);
    }
    fclose(fp);
    return 1;
}

int longitudCadena(char * cad)
{
    int longitud = 0;

    while ( *(cad + longitud) != '\n' && *(cad + longitud) >= 'A' && *(cad + longitud) <= 'z' )
    {
        longitud++;
    }

    return longitud;
}

int buscarEnArchivo(char * linea, char * palabra, int * res, int *posicionFila, int * posicionLinea, int * posicionAux)
{

    int i = 0, j = 0, k = 0, longitud = 0;

    *posicionAux+=1;

    char * aux;
    char * aux1;

    longitud = longitudCadena(palabra);

    while ( *(linea + i) != '\n')
    {

        if ( *(linea + i) == *(palabra + j) )
        {

            aux = (linea + i);
            aux1 = (palabra + j);

            while ( *(aux + k) == *(aux1 + k) && k != longitud )
            {

                k++;

            }

            if ( k == longitud && ( *(aux + k) == 32 || *(aux + k) == ' ' ||  *(aux + k) == '\n') && (*(aux - 1 ) == ' ' || *(aux - 1 ) == 0 )  )
            {

                *res += 1;
                *posicionLinea = i;
                *posicionFila = *posicionAux;

            }

            k = 0;

        }
        i++;

    }

    return *res;
}
